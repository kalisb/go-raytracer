package main

import (
	"unsafe"

	"github.com/veandco/go-sdl2/sdl"
)

var (
	screen sdl.Surface
	window sdl.Window
)

/// returns the frame width
func frameWidth() int32 {
	return screen.W
}

/// returns the frame height
func frameHeight() int32 {
	return screen.H
}

func initGraphics(frameWidth, frameHeight int32) {
	// initializes SDL
	if err := sdl.Init(sdl.INIT_VIDEO); err != nil {
		panic(err)
	}
}

/// displays a VFB (virtual frame buffer) to the real framebuffer, with the necessary color clipping
func displayVFB(vfb [VFB_MAX_SIZE][VFB_MAX_SIZE]Color) {
	window, err := sdl.CreateWindow("test", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		RESX, RESY, sdl.WINDOW_SHOWN)
	if err != nil {
		panic(err)
	}
	screen, err := window.GetSurface()
	if err != nil {
		panic(err)
	}
	pixels := make([]uint32, RESX*RESY)
	k := 0
	for y := 0; y < RESY; y++ {
		for x := 0; x < RESX; x++ {
			pixels[k] = vfb[x][y].toRGB32(16, 8, 0)
			k++
		}
	}
	image, err := sdl.CreateRGBSurfaceFrom(unsafe.Pointer(&pixels[0]), int32(RESX), int32(RESY), 32, RESX*int(unsafe.Sizeof(pixels[0])), 0, 0, 0, 0)
	if err != nil {
		panic(err)
	}
	err = image.Blit(nil, screen, nil)
	if err != nil {
		panic(err)
	}
	image.Free()

	window.UpdateSurface()
}

func waitForUserExit() {
	running := true
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				println("Quit")
				running = false
				break
			}
		}
	}
}

func closeGraphics() {
	sdl.Quit()
	window.Destroy()
}
