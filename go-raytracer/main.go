package main

import (
	"fmt"
)

const (
	VFB_MAX_SIZE = 1920
	RESX         = 800
	RESY         = 600
	PI           = 3.141592653589793238
	INF          = 1e99
)

var vfb [VFB_MAX_SIZE][VFB_MAX_SIZE]Color

func render() {
	for j := RESY - 1; j >= 0; j-- {
		for i := 0; i < RESX; i++ {
			vfb[i][j] = Color{float64(i) / float64(RESX), float64(j) / float64(RESY), 0.2}
		}
	}
}

func main() {
	initGraphics(RESX, RESY)
	render()
	displayVFB(vfb)
	waitForUserExit()
	closeGraphics()
	fmt.Printf("Exited cleanly\n")
}
