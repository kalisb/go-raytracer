package main

import (
	"math"
)

// Color represents a color, using floatingpoint components in [0..1]
type Color struct {
	R, G, B float64
}

// NewColor constructs a color from floatingpoint values
func NewColor(r, g, b float64) *Color {
	return &Color{r, g, b}
}

// Add accumulates some color to the current
func (c Color) Add(other Color) Color {
	return Color{c.R + other.R, c.G + other.G, c.B + other.B}
}

// Mult multiplies a color by some multiplier
func (c Color) Mult(multiplier float64) Color {
	return Color{c.R * multiplier, c.G * multiplier, c.B * multiplier}
}

func (c Color) toRGB32(redShift, greenShift, blueShift uint32) uint32 {
	R := uint32(math.Min(255.0, c.R*255.99))
	G := uint32(math.Min(255.0, c.G*255.99))
	B := uint32(math.Min(255.0, c.B*255.99))
	return ((R & 0xFF) << 16) | ((G & 0xFF) << 8) | (B & 0xFF)
}

func convertTo8bit(x float64) uint32 {
	if x < 0 {
		x = 0
	}
	if x > 1 {
		x = 1
	}
	return uint32(math.Ceil(x * 255.0))
}
